/*
 * Group : XSTS/TAD
 * Project : World Registry
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.tad.registry.world.directory.af;

public class AfricanCountries {
    public static String NORTH[] [] = {
            {"dz","Algeria"},
            {"eg","Egypt"},
            {"ly","Libya"},
            {"ma","Morocco"},
            {"tn","Tunisia"}
    };

    public static String EAST[] [] = {

            {"bi","Burundi"},
            {"km","Comoros"},
            {"dj","Djibouti"},
            {"er","Eritrea"},
            {"et","Ethiopia"},
            {"ke","Kenya"},
            {"mg","Madagascar"},
            {"mw","Malawi"},
            {"mz","Mozambique"},
            {"rw","Rwanda"},
            {"so","Somalia"},
            {"ss","South Sudan"},
            {"sd","Sudan"},
            {"tz","Tanzania"},
            {"ug","Uganda"}
    };

    public static String CENTER[] [] = {
            {"ao","Angola"}
    };

    public static String SOUTH[] [] = {
            {"ao","Botswana"},
            {"na","Namibia"},
            {"zm","Zambia"},
            {"zw","Zimbabwe"},
            {"za","South Africa"}
    };

    public static String WEST[] [] = {
            {"ng","Nigeria"}
    };
}
