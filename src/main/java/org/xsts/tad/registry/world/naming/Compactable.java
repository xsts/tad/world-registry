/*
 * Group : XSTS/TAD
 * Project : World Registry - identifies and retrieves a transit agency
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.tad.registry.world.naming;

public interface Compactable {
    String translate(String oldName);
    String toSmallCaps(String oldName);
}
