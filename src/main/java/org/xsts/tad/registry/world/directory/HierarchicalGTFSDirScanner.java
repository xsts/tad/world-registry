/*
 * Group : XSTS/TAD
 * Project : World Registry - identifies and retrieves a transit agency
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.tad.registry.world.directory;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class HierarchicalGTFSDirScanner {

    HierarchicalGTFSAgency gtfs;
    List<String> dirList;
    List<String> agencyList;

    //Map<String, HierarchicalGTFSAgency> agencyList;

    public HierarchicalGTFSAgency gtfs() { return this.gtfs;}

    public HierarchicalGTFSDirScanner() {
        reset();
    }

    public void init() {
        gtfs = new HierarchicalGTFSAgency();
        dirList = new ArrayList<>();
        agencyList = new ArrayList<>();
    }

    public void reset() {
        gtfs = null;
        dirList = null;
        agencyList = null;
    }

    public void scan(String path) {
        reset();
        init();

        buildDirList(path);
        buildAgencyList(path);
        buildAgencyHierarchy();
        gtfs.segment("gtfs");
        consolidateAgencyHierarchy(gtfs);

    }

    private void consolidateAgencyHierarchy(HierarchicalGTFSAgency element) {
        if (element.hasChildren()) {
            for (String key : element.children().keySet()){
                HierarchicalGTFSAgency child = element.child(key);
                child.parent(element);
                consolidateAgencyHierarchy(child);
            }
        }
    }

    private void buildAgencyHierarchy() {
       for (String relativePath: agencyList) {
           String[] tokens = relativePath.split("/");
           HierarchicalGTFSAgency relAgency = gtfs;
           int count = 1;
           while ( count < tokens.length) {
               String segment = tokens[count];
               if ( relAgency.child(segment) == null) {
                   HierarchicalGTFSAgency agency = new HierarchicalGTFSAgency();
                   agency.segment(segment);
                   relAgency.add(agency);
               }
               relAgency = relAgency.child(segment);
               count ++;
           }


       }
    }


    private void buildAgencyList(String path) {
        for (String childPath : dirList){

            if (detectAgency(childPath)){
                String relativePath = getRelativePath(path, childPath );
                agencyList.add(relativePath);
            }
        }
    }

    private boolean detectAgency(String childPath) {
        File[] children = new File(childPath).listFiles();
        boolean hasAgency = false;
        boolean hasRoutes = false;
        boolean hasStops = false;
        boolean hasTrips = false;
        boolean hasStopTimes = false;
        boolean hasCalendar = false;
        if (children != null ) {
            for (File child : children) {
                if ( child.isDirectory())
                    continue;
                String childName = child.getName();

                if (childName.equalsIgnoreCase("agency.txt"))
                    hasAgency = true;
                else if (childName.equalsIgnoreCase("routes.txt"))
                    hasRoutes = true;
                else if (childName.equalsIgnoreCase("trips.txt"))
                    hasTrips = true;
                else if (childName.equalsIgnoreCase("stops.txt"))
                    hasStops = true;
                else if (childName.equalsIgnoreCase("stop_times.txt"))
                    hasStopTimes = true;
                else if (childName.equalsIgnoreCase("calendar.txt"))
                    hasCalendar = true;


            }

        }

        if (hasAgency && hasRoutes && hasStops && hasStopTimes && hasTrips && hasCalendar) {
            // This is most likely a valid agency
            //System.out.println("Dir " + childPath + " contains GTFS data");
            return true;
        }

        return false;
    }

    private String getRelativePath(String rootPath, String childPath) {
        String[] rootTokens = rootPath.split("/");
        String[] childTokens = childPath.split("/");

        int count = 0;
        while (count < rootTokens.length && count < childTokens.length) {
            count ++;
        }

        if ( count >= childTokens.length) {
            System.out.println("Something went wrong with the relative path tokens");
            return "---------";
        }

        StringBuilder sb = new StringBuilder();
        //sb.append("/gtfs");

        while ( count < childTokens.length) {
            sb.append("/").append(childTokens[count]);
            count ++;
        }

        return sb.toString();
    }


    private void buildDirList(String currentPath) {
        File[] children = new File(currentPath).listFiles();
        if (children != null ) {
            for (File child : children) {
                String childName = child.getName();
                if ( childName.compareTo(".") == 0)
                    continue;
                if ( childName.compareTo("..") == 0)
                    continue;
                if (child.isDirectory()) {
                    String fullPath = child.getAbsolutePath();
                    dirList.add(fullPath);
                    buildDirList(fullPath);
                }
            }
        }
    }
}
