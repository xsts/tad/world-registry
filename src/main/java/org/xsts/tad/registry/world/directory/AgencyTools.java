/*
 * Group : XSTS/TAD
 * Project : World Registry - identifies and retrieves a transit agency
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.tad.registry.world.directory;

public class AgencyTools {
    // Maybe I should split this class in two parts (add vs. addReplace)
    static AddressableAgency internalRegister(AddressableAgency parentAgency,
                                              String segment,
                                              String name,
                                              String hint) {
        DynamicAgency dynamicAgency = new DynamicAgency();
        dynamicAgency
                        .pathSegment(segment)
                        .parentAgency(parentAgency);
        dynamicAgency.automaticPath();
        dynamicAgency
                        .name(name)
                        .hint(hint);

        parentAgency.customizedAdd(dynamicAgency);

        return dynamicAgency;
    }

    public static AddressableAgency register(AddressableAgency parentAgency,
                                             String segment) {
        return internalRegister(parentAgency,
                                segment,
                            null,
                            null);
    }

    public static AddressableAgency register(AddressableAgency parentAgency,
                                             String segment,
                                             String name) {
        return internalRegister(parentAgency,
                                segment,
                                name,
                            null);
    }

    public static AddressableAgency register(AddressableAgency parentAgency,
                                             String segment,
                                             String name,
                                             String hint) {
        return internalRegister(parentAgency,
                                segment,
                                name,
                                hint);
    }

    public static AddressableAgency groupRegister(AddressableAgency parentAgency, final String[][] data) {
        for ( int pos = 0; pos < data.length; ++pos){
            final String [] element = data[pos];
            final String segment = element[CountryRegistryFields.INTERNET_PREFIX];
            final String country = element[CountryRegistryFields.COUNTRY_NAME];
            String agencyName = country + " Agency";
            String hintText = "agency generated from bulk data";
            internalRegister(parentAgency,segment,agencyName, hintText );
        }
        return parentAgency;
    }

    public static AddressableAgency internalRegisterWithReplace(AddressableAgency parentAgency, AddressableAgency agency) {
        parentAgency.customizedAddWithReplace(agency);
        return parentAgency;
    }
}
