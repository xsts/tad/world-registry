/*
 * Group : XSTS/TAD
 * Project : World Registry - identifies and retrieves a transit agency
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.tad.registry.world.directory.eu;

import org.xsts.tad.registry.world.directory.AddressableAgency;
import org.xsts.tad.registry.world.directory.AgencyTools;
import org.xsts.tad.registry.world.directory.RootGTFSAgency;

public class EuropeanAgency extends AddressableAgency {
    public static final String NAME = "European Agency";

    public static final String PATH_SEGMENT = "eu";

    public  EuropeanAgency() {
        super();
        AgencyTools.groupRegister(this, EuropeanCountries.NORTH);
        AgencyTools.groupRegister(this, EuropeanCountries.CENTER);
        AgencyTools.groupRegister(this, EuropeanCountries.EAST);
        AgencyTools.groupRegister(this, EuropeanCountries.WEST);
        AgencyTools.groupRegister(this, EuropeanCountries.SOUTH);
    }

    public static String path() {
        return RootGTFSAgency.path() + "/" + RootGTFSAgency.PATH_SEGMENT;
    }

    @Override
    public String pathTo() {
        return EuropeanAgency.path();
    }

    @Override
    public String signature() {
        return EuropeanAgency.PATH_SEGMENT;
    }

    @Override
    public String name() {
        return NAME;
    }
}
