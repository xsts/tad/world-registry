/*
 * Group : XSTS/TAD
 * Project : World Registry - identifies and retrieves a transit agency
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.tad.registry.world.directory;

public class DynamicAgency extends AddressableAgency {
    String pathSegment;
    String path;
    String pathTo;
    String signature;
    String name;
    String hint;

    AddressableAgency pluggableAgency;

    public DynamicAgency() {
        super();
    }

    public DynamicAgency hint( String hint) { this.hint = hint; return this;}
    public DynamicAgency name( String name) { this.name = name; return this;}
    public DynamicAgency automaticPath() { this.path = parentAgency.pathTo() + "/" + parentAgency.signature(); return this;}
    public DynamicAgency pathSegment(String pathSegment) { this.pathSegment = pathSegment; return this;}

    public static String path() {  return "";  }

    @Override
    public String pathTo() {
        return pluggableAgency != null ? pluggableAgency.pathTo() : this.path;
    }

    @Override
    public String signature() {
        return pluggableAgency != null ? pluggableAgency.signature() : pathSegment;
    }

    @Override
    public String name() {
        return pluggableAgency != null ? pluggableAgency.name() : this.name;
    }

    // this field will store information in CSV-like format : internal-id:100,added:20200416,etc
    public String hint() { return this.hint;}

     void pluggableAgency(AddressableAgency pluggableAgency) { this.pluggableAgency = pluggableAgency; }
     AddressableAgency pluggableAgency() { return this.pluggableAgency;}

    public static void plug(DynamicAgency proxy,AddressableAgency plug) {
        plug.parentAgency(proxy.parentAgency());
        proxy.pluggableAgency(plug);
    }
}
