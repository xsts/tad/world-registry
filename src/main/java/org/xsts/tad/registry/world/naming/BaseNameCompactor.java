/*
 * Group : XSTS/TAD
 * Project : World Registry - identifies and retrieves a transit agency
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.tad.registry.world.naming;

import java.util.HashMap;
import java.util.Map;

public class BaseNameCompactor implements Compactable{
    public static final String COMPACT_TRANSLATOR = "bnc.translator";
    Map<String, String> translator;

    public BaseNameCompactor() {
        translator = new HashMap<>();
    }

    public void addTranslation(String key, String value) {
        translator.put(key, value);
    }

    @Override
    public String translate(String oldName) {
        if ( translator.containsKey(oldName))
            return translator.get(oldName);
        else
            return oldName;
    }

    @Override
    public String toSmallCaps(String oldName) {
        String lowCase = oldName.toLowerCase();
        StringBuilder sb = new StringBuilder();

        String[] tokens = lowCase.split(" ");
        for ( int i = 0; i < tokens.length; i++){
            String token= tokens[i].toUpperCase();

            if ( token.length() > 1) {

                String tok1 = token.substring(0,1);
                String tok2 = token.substring(1).toLowerCase();
                token = tok1 + tok2;
                tokens[i] = token;
            }


            if ( i > 0)
                sb.append(" ");
            sb.append(translate(tokens[i]));
        }

        return sb.toString();
    }
}
