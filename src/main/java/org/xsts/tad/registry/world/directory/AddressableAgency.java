/*
 * Group : XSTS/TAD
 * Project : World Registry - identifies and retrieves a transit agency
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.tad.registry.world.directory;

import java.awt.Color;
import java.util.*;
import java.util.List;
import java.util.regex.Pattern;

public abstract class AddressableAgency implements  Addressable{
    protected Map<String, AddressableAgency> agencies;
    protected Map<String, AddressableAgency> uniqueSignatures;
    protected AddressableAgency parentAgency = null;

    protected AddressableAgency() {
        agencies = new HashMap<>();
        uniqueSignatures = new HashMap<>();
    }

    public AddressableAgency parentAgency() { return this.parentAgency;}
    public AddressableAgency parentAgency(AddressableAgency parentAgency) { this.parentAgency = parentAgency; return this;}

    public Set<String> signatureKeys() { return uniqueSignatures.keySet(); }
    public AddressableAgency getByUniqueSignature(String localSignature) {
        return uniqueSignatures.get(localSignature);
    }



    public void customizedAdd(AddressableAgency agency) {
        this.add(agency);
        agency.parentAgency(this);
        propagateUniqueSignature(agency);
    }


    public void customizedAddWithReplace(AddressableAgency agency) {
        this.addWithReplace(agency);
        agency.parentAgency(this);
        propagateUniqueSignatureWithReplace(agency);

    }

    public void propagateUniqueSignature(AddressableAgency agency) {
        List<String> segments = new ArrayList<>();
        AddressableAgency theParent = agency.parentAgency();
        while (theParent != null) {
            segments.add(0, theParent.signature());
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < segments.size(); i++) {
                sb.append(segments.get(i)).append(".");
            }
            sb.append(agency.signature());

            theParent.addUniqueSignature(sb.toString(), agency);

            theParent = theParent.parentAgency();
        }
    }

    public void propagateUniqueSignatureWithReplace(AddressableAgency agency) {
        List<String> segments = new ArrayList<>();
        AddressableAgency theParent = agency.parentAgency();
        while (theParent != null) {
            segments.add(0, theParent.signature());
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < segments.size(); i++) {
                sb.append(segments.get(i)).append(".");
            }
            sb.append(agency.signature());

            theParent.addUniqueSignatureWithReplace(sb.toString(), agency);

            theParent = theParent.parentAgency();
        }
    }

    protected void  addUniqueSignature(String anotherSignature, AddressableAgency agency) {
        if ( !uniqueSignatures.containsKey(anotherSignature)){
            uniqueSignatures.put(anotherSignature, agency);
        }
    }

    protected void  addUniqueSignatureWithReplace(String anotherSignature, AddressableAgency agency) {
            uniqueSignatures.put(anotherSignature, agency);
    }


    protected void add(AddressableAgency agency) {
        if ( !agencies.containsKey(agency.signature())) {
            agencies.put(agency.signature(), agency);
            agency.parentAgency(this);
        }

        String localSignature = this.signature() + "." + agency.signature();
        if ( !uniqueSignatures.containsKey(localSignature)) {
            uniqueSignatures.put(localSignature, agency);
        }

        // and the unique signatures
        Set<String> tempSignatures = agency.signatureKeys();
        for (String aSignature : tempSignatures) {
            String anotherSignature = this.signature() + "." + aSignature;
            if ( !uniqueSignatures.containsKey(anotherSignature)){
                uniqueSignatures.put(anotherSignature, agency.getByUniqueSignature(aSignature));
            }
        }
    }

    protected void addWithReplace(AddressableAgency agency) {
        agencies.put(agency.signature(), agency);


        String localSignature = this.signature() + "." + agency.signature();
        uniqueSignatures.put(localSignature, agency);

        // and the unique signatures
        Set<String> tempSignatures = agency.signatureKeys();
        for (String aSignature : tempSignatures) {
            String anotherSignature = this.signature() + "." + aSignature;
            uniqueSignatures.put(anotherSignature, agency.getByUniqueSignature(aSignature));
        }
    }


    public Addressable find(String signature){
        Addressable result = null;

        if ( signature.compareTo(this.signature()) == 0) {
            result = this;
        } else {
            for (String key : agencies.keySet()){
                Addressable addressable = agencies.get(key).find(signature);
                if ( addressable != null) {
                    result = addressable;
                    break;
                }
            }
        }

        return result;
    }

    public String toString() {
        return this.pathTo() + "/" +this.signature() ;
    }


    @Override
    public boolean hasSpecialTransitLineDrawing() { return false;}

    @Override
    public boolean useCircle(String routeShortName){ return false;}

    @Override
    public boolean useRimCircle(String routeShortName){ return false;}

    @Override
    public boolean useRect(String routeShortName){ return false;}

    @Override
    public boolean useUnderLine(String routeShortName){ return false;}

    @Override
    public Color getBackColor(String routeShortName){ return Color.black;}

    @Override
    public boolean hasCorruptedData() {
        return false;
    }

    public  AddressableAgency findByUniqueSignature(String signature) {
        AddressableAgency result = this.getByUniqueSignature(signature);
        if ( result != null)
            return result;

        for (String key : uniqueSignatures.keySet()){
            AddressableAgency locator = uniqueSignatures.get(key);
            AddressableAgency addressable = locator.findByUniqueSignature(signature);
            if ( addressable != null) {
                result = addressable;
                break;
            }
        }

        return result;
    }


    public  AddressableAgency findByStarSignature(String signature) {
        String[] tokenSignature = signature.split(Pattern.quote("."));
        return findByStarSignature(tokenSignature);
    }


    public  AddressableAgency findByStarSignature(String []signature) {
        AddressableAgency result = null;
        for (String key : uniqueSignatures.keySet()){
            AddressableAgency locator = uniqueSignatures.get(key);
            String[] tokenKey = key.split(Pattern.quote("."));
            if ( tokenKey.length == signature.length) {
                if ( tokenKey.length == 2 && signature[0].compareTo("*") == 0) {
                    if ( tokenKey[1].compareTo(signature[1]) == 0) {
                        result = locator;
                    }
                } else if (tokenKey.length == 3 && signature[1].compareTo("*") == 0) {
                    if ( tokenKey[0].compareTo(signature[0]) == 0 && tokenKey[2].compareTo(signature[2]) == 0 ) {
                        result = locator;
                    }
                } else if (tokenKey.length == 3 && signature[0].compareTo("*") == 0) {
                    if ( tokenKey[1].compareTo(signature[1]) == 0 && tokenKey[2].compareTo(signature[2]) == 0 ) {
                        result = locator;
                    }
                } else if (tokenKey.length == 4 && signature[1].compareTo("*") == 0 && signature[2].compareTo("*") == 0) {
                    if ( tokenKey[0].compareTo(signature[0]) == 0 && tokenKey[3].compareTo(signature[2]) == 0 ) {
                        result = locator;
                    }
                } else if (tokenKey.length == 4  && signature[0].compareTo("*") == 0 ) {
                    if ( tokenKey[1].compareTo(signature[1]) == 0 && tokenKey[2].compareTo(signature[2]) == 0 && tokenKey[3].compareTo(signature[3]) == 0 ) {
                        result = locator;
                    }
                } else if (tokenKey.length == 5 && signature[1].compareTo("*") == 0 && signature[2].compareTo("*") == 0 && signature[3].compareTo("*") == 0) {
                    if ( tokenKey[0].compareTo(signature[0]) == 0 && tokenKey[3].compareTo(signature[4]) == 0 ) {
                        result = locator;
                    }
                } else {
                    return null;
                }
            } else {
                AddressableAgency addressable = locator.findByStarSignature(signature);
                if ( addressable != null) {
                    result = addressable;
                }
            }
            if (result != null)
                break;
        }

        return result;
    }

    public String getFullyQualifiedDomainName(AddressableAgency candidate) {
        String domain = "not found";
        for (String key : uniqueSignatures.keySet()){
            AddressableAgency locator = uniqueSignatures.get(key);
            if ( locator  == candidate) {
                domain = key;
                break;
            }
        }
        return domain;
    }

}
