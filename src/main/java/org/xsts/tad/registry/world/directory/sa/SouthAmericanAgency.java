/*
 * Group : XSTS/TAD
 * Project : World Registry - identifies and retrieves a transit agency
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.tad.registry.world.directory.sa;

import org.xsts.tad.registry.world.directory.AddressableAgency;
import org.xsts.tad.registry.world.directory.AgencyTools;
import org.xsts.tad.registry.world.directory.RootGTFSAgency;

public class SouthAmericanAgency extends AddressableAgency {
    public static final String NAME = "South American Agency";

    public static final String PATH_SEGMENT = "sa";

    public SouthAmericanAgency() {
        super();
        AgencyTools.groupRegister(this, SouthAmericanCountries.DATA);

    }

    public static String path() {
        return RootGTFSAgency.path() + "/" + RootGTFSAgency.PATH_SEGMENT;
    }

    @Override
    public String pathTo() {
        return SouthAmericanAgency.path();
    }

    @Override
    public String signature() {
        return SouthAmericanAgency.PATH_SEGMENT;
    }

    @Override
    public String name() {
        return NAME;
    }

}
