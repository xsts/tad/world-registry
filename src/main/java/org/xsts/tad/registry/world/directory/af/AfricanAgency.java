/*
 * Group : XSTS/TAD
 * Project : World Registry
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.tad.registry.world.directory.af;

import org.xsts.tad.registry.world.directory.AddressableAgency;
import org.xsts.tad.registry.world.directory.AgencyTools;
import org.xsts.tad.registry.world.directory.RootGTFSAgency;

public class AfricanAgency extends AddressableAgency {

    public static final String NAME = "African Agency";
    public static final String PATH_SEGMENT = "af";

    public  AfricanAgency() {
        super();
        AgencyTools.groupRegister(this, AfricanCountries.NORTH);
        AgencyTools.groupRegister(this, AfricanCountries.SOUTH);
        AgencyTools.groupRegister(this, AfricanCountries.EAST);
        AgencyTools.groupRegister(this, AfricanCountries.WEST);
        AgencyTools.groupRegister(this, AfricanCountries.CENTER);
    }

    public static String path() {
        return RootGTFSAgency.path() + "/" + RootGTFSAgency.PATH_SEGMENT;
    }

    @Override
    public String pathTo() {
        return AfricanAgency.path();
    }

    @Override
    public String signature() {
        return AfricanAgency.PATH_SEGMENT;
    }

    @Override
    public String name() {
        return NAME;
    }


}
