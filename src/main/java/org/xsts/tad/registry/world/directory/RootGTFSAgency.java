/*
 * Group : XSTS/TAD
 * Project : World Registry - identifies and retrieves a transit agency
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.tad.registry.world.directory;

import org.xsts.tad.registry.world.directory.af.AfricanAgency;
import org.xsts.tad.registry.world.directory.as.AsianAgency;
import org.xsts.tad.registry.world.directory.eu.EuropeanAgency;
import org.xsts.tad.registry.world.directory.na.NorthAmericanAgency;
import org.xsts.tad.registry.world.directory.oa.OceanianAgency;
import org.xsts.tad.registry.world.directory.sa.SouthAmericanAgency;

public class RootGTFSAgency extends AddressableAgency {
    public static final String PATH_SEGMENT = "gtfs";
    public static final String NAME = "Root GTFS Agency";

    public RootGTFSAgency() {
        super();
        add(new OceanianAgency());
        add(new AfricanAgency());
        add(new AsianAgency());
        add(new EuropeanAgency());
        add(new NorthAmericanAgency());
        add(new SouthAmericanAgency());
    }

    public static String path() {
        return "";
    }

    @Override
    public String pathTo() {
        return RootGTFSAgency.path();
    }

    @Override
    public String signature() {
        return RootGTFSAgency.PATH_SEGMENT;
    }

    @Override
    public String name() {
        return NAME;
    }
}
