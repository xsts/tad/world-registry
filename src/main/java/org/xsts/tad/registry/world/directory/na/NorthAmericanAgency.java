/*
 * Group : XSTS/TAD
 * Project : World Registry - identifies and retrieves a transit agency
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.tad.registry.world.directory.na;

import org.xsts.tad.registry.world.directory.AddressableAgency;
import org.xsts.tad.registry.world.directory.AgencyTools;
import org.xsts.tad.registry.world.directory.RootGTFSAgency;

public class NorthAmericanAgency  extends AddressableAgency {
    public static final String NAME = "North American Agency";

    public static final String PATH_SEGMENT = "na";

    public NorthAmericanAgency() {
        super();
        AgencyTools.groupRegister(this, NorthAmericanCountries.DATA);

    }

    public static String path() {
        return RootGTFSAgency.path() + "/" + RootGTFSAgency.PATH_SEGMENT;
    }

    @Override
    public String pathTo() {
        return NorthAmericanAgency.path();
    }

    @Override
    public String signature() {
        return NorthAmericanAgency.PATH_SEGMENT;
    }

    @Override
    public String name() {
        return NAME;
    }

}
