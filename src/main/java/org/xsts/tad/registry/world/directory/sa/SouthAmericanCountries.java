/*
 * Group : XSTS/TAD
 * Project : World Registry - identifies and retrieves a transit agency
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */package org.xsts.tad.registry.world.directory.sa;

public class SouthAmericanCountries {
    public static String DATA[] [] = {
            {"ar","Argentina"},
            {"bo","Bolivia"},
            {"br","Brazil"},
            {"cl","Chile"},
            {"co","Colombia"},
            {"ec","Ecuador"},
            {"pe","Peru"},
            {"py","Paraguay"},
            {"uy","Uruguay"},
            {"ve","Venezuela"}
    };
}
