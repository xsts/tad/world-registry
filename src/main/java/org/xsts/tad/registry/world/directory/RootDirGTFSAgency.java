/*
 * Group : XSTS/TAD
 * Project : World Registry - identifies and retrieves a transit agency
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.tad.registry.world.directory;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class RootDirGTFSAgency extends AddressableAgency {
    public static final String PATH_SEGMENT = "gtfs";
    public static final String NAME = "Root Dir GTFS Agency";

    public RootDirGTFSAgency() {
        super();
    }

    public static String path() {
        return "";
    }

    @Override
    public String pathTo() {
        return RootGTFSAgency.path();
    }

    @Override
    public String signature() {
        return RootGTFSAgency.PATH_SEGMENT;
    }

    @Override
    public String name() {
        return NAME;
    }

    public List<String> scanDir(String curentPath, List<String> currentList) {
        List<String> result = currentList;
        if (result == null ) {
            result  = new ArrayList<>();
        }

        File[] children = new File(curentPath).listFiles();
        if (children != null ) {
            for (File child : children) {
                String childName = child.getName();
                if ( childName.compareTo(".") == 0)
                    continue;
                if ( childName.compareTo("..") == 0)
                    continue;
                if (child.isDirectory()) {
                    String fullPath = child.getAbsolutePath();
                    currentList.add(fullPath);
                    scanDir(fullPath,currentList );
                }
            }
        }

        return currentList;
    }



}
