/*
 * Group : XSTS/TAD
 * Project : World Registry - identifies and retrieves a transit agency
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.tad.registry.world.directory.oa;

import org.xsts.tad.registry.world.directory.AddressableAgency;
import org.xsts.tad.registry.world.directory.RootGTFSAgency;

public class OceanianAgency extends AddressableAgency {
    public static final String NAME = "Oceanian Agency";

    public static final String PATH_SEGMENT = "oa";

    public  OceanianAgency() {
        super();
    }

    public static String path() {
        return RootGTFSAgency.path() + "/" + RootGTFSAgency.PATH_SEGMENT;
    }

    @Override
    public String pathTo() {
        return OceanianAgency.path();
    }

    @Override
    public String signature() {
        return OceanianAgency.PATH_SEGMENT;
    }

    @Override
    public String name() {
        return NAME;
    }
}
