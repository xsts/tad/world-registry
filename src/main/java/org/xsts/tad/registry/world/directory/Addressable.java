/*
 * Group : XSTS/TAD
 * Project : World Registry - identifies and retrieves a transit agency
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.tad.registry.world.directory;

import java.awt.Color;

public interface Addressable {
    String pathTo();
    String signature();
    String name();
    boolean hasSpecialTransitLineDrawing();
    boolean useCircle(String routeShortName);
    boolean useRimCircle(String routeShortName);

    boolean useRect(String routeShortName);
    boolean useUnderLine(String routeShortName);
    Color getBackColor(String routeShortName);
    boolean hasCorruptedData();
}
