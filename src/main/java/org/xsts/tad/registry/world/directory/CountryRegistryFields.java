/*
 * Group : XSTS/TAD
 * Project : World Registry - identifies and retrieves a transit agency
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.tad.registry.world.directory;

public class CountryRegistryFields {
    public static Integer INTERNET_PREFIX = 0;
    public static Integer COUNTRY_NAME = INTERNET_PREFIX + 1;
}
