/*
 * Group : XSTS/TAD
 * Project : World Registry - identifies and retrieves a transit agency
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.tad.registry.world.directory.na;

public class NorthAmericanCountries {
    public static String DATA[] [] = {
            {"ca","Canada"},
            {"us","United States"},
            {"mx","Mexico"}
    };
}
