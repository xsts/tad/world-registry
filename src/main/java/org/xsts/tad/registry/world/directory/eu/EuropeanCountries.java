/*
 * Group : XSTS/TAD
 * Project : World Registry - identifies and retrieves a transit agency
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.tad.registry.world.directory.eu;

public class EuropeanCountries {
    public static String NORTH[] [] = {
            {"is","Iceland"},
            {"no","Norway"},
            {"se","Sweden"},
            {"fi","Finland"},
            {"dk","Denmark"},
            {"is","Iceland"},
            {"ee","Estonia"},
            {"lv","Latvia"},
            {"lt","Lithuania"}
    };

    public static String WEST[] [] = {
            {"fr","France"},
            {"be","Belbium"},
            {"lu","Luxemburg"},
            {"nl","Netherlands"},
            {"uk","United Kingdom"},
            {"ie","Ireland"}
    };

    public static String CENTER[] [] = {
            {"cz","Czech Republic"},
            {"sk","Slovakia"},
            {"hu","Hungary"},
            {"pl","Poland"},
            {"de","Germany"},
            {"ch","Switzerland"},
            {"at","Austria"},
            {"si","Slovenia"}
    };


    public static String SOUTH[] [] = {
            {"es","Spain"},
            {"pt","Portugal"},
            {"it","Italy"},
            {"gr","Greece"}
    };

    public static String EAST[] [] = {
            {"ro","Romania"},
            {"rs","Serbia"},
            {"bg","Bulgaria"},
            {"hr","Croatia"},
            {"al","Albania"}
    };
}
