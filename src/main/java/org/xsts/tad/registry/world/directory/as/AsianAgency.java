/*
 * Group : XSTS/TAD
 * Project : World Registry
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.tad.registry.world.directory.as;

import org.xsts.tad.registry.world.directory.AddressableAgency;
import org.xsts.tad.registry.world.directory.RootGTFSAgency;


public class AsianAgency extends AddressableAgency {
    public static final String NAME = "Asian Agency";

    public static final String PATH_SEGMENT = "as";

    public  AsianAgency() {
        super();
    }

    public static String path() {
        return RootGTFSAgency.path() + "/" + RootGTFSAgency.PATH_SEGMENT;
    }

    @Override
    public String pathTo() {
        return AsianAgency.path();
    }

    @Override
    public String signature() {
        return AsianAgency.PATH_SEGMENT;
    }

    @Override
    public String name() {
        return NAME;
    }
}
