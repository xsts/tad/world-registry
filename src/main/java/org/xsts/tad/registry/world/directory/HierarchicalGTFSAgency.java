/*
 * Group : XSTS/TAD
 * Project : World Registry - identifies and retrieves a transit agency
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.tad.registry.world.directory;

import java.util.HashMap;
import java.util.Map;

public class HierarchicalGTFSAgency extends AddressableAgency{

    private String segment;
    private HierarchicalGTFSAgency parent;
    private Map<String,HierarchicalGTFSAgency > children;

    public HierarchicalGTFSAgency() {
        super();
        parent = null;
        children = null;
        segment = null;
    }

    public boolean hasChildren () {
        return children != null && children.size() > 0;
    }

    public HierarchicalGTFSAgency parent(HierarchicalGTFSAgency parent) {
        this.parent = parent;
        return this;
    }

    public HierarchicalGTFSAgency parent() {
        return this.parent;
    }


    public HierarchicalGTFSAgency add(HierarchicalGTFSAgency child) {
        if ( children == null) {
            children = new HashMap<>();
        }

        children.put(child.segment(), child);
        return this;
    }

    public String segment() {
        return this.segment;
    }

    public HierarchicalGTFSAgency segment(String segment) {
        this.segment = segment;
        return this;
    }

    public Map<String,HierarchicalGTFSAgency > children() {
        return this.children;
    }

    public static String path() {
        return null;
    }

    public HierarchicalGTFSAgency child(String segment){
        if (children == null)
            return null;
        if ( children.containsKey(segment)) {
            return children.get(segment);
        }

        return null;
    }

    public String fqdn() {
            return pathTo() + "/" + segment();
    }

    @Override
    public String pathTo() {
        if (parent == null && segment == null)
            return null;

        if (parent != null && segment != null) {
            return parent.pathTo() + "/" + parent.segment();
        }

        return "";
    }

    @Override
    public String signature() {
        return this.segment;
    }

    @Override
    public String name() {
        return "-";
    }

    public HierarchicalGTFSAgency findAgency(String code) {
        if (fqdn().endsWith(code))
            return this;
        if (hasChildren()){
            for (String key: children.keySet()) {
                HierarchicalGTFSAgency temp = child(key);
                HierarchicalGTFSAgency candidate = temp.findAgency(code);
                if ( candidate != null)
                    return candidate;
            }
        }

        return null;

    }

}
